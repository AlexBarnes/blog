---
title: Mounting NAS storage into Kubernetes
date: 2019-06-23
tags: ["kubernetes"]
---

As I mentioned in my post about my home setup, I run Kubernetes on a cluster of Intel NUCs and a Synology DS1019+ for storage. This post outlines how you can mount NAS, with a specific focus on a Synology DS series, storage into a Kubernetes cluster. While the NAS setup is different from platform to platform, the Kubernetes configuration should be the same.

Full disclosure, Synology provided me some hardware free of cost.

First, you'll need to enable [NFS](https://en.wikipedia.org/wiki/Network_File_System) on your NAS and setup permissions on a Shared Folder. Synology support has [a great doc on how to do this](https://www.synology.com/en-us/knowledgebase/DSM/tutorial/File_Sharing/How_to_access_files_on_Synology_NAS_within_the_local_network_NFS), so I won't repeat all of it here. Make sure while setting up permissions that all of your Kubernetes hosts are included in either the host name wildcard or network [CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing) used for `Hostname of IP*`. For instance, my hosts are all 192.168.0.\* so I entered the CIDR `192.168.0.0/24` when setting permissions.

![NFS Permission](https://i.imgur.com/CuVXyCG.png)

In order to use the storage in Kubernetes, we'll be using a [Persistent Volume and Persistent Volume Claims](https://kubernetes.io/docs/concepts/storage/persistent-volumes/). A Persistent Volume represents a piece of persistent storage available to your cluster, in our case an NFS share. A Persistent Volume Claim is a request to consume storage within a pod, in our case to mount the NFS share.

First, let's take a look at the PersistentVolume. Here's what the YAML should look like:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-share
spec:
  storageClassName: manual
  capacity:
    storage: 500Gi
  persistentVolumeReclaimPolicy: Retain
  accessModes:
    - ReadWriteMany
  nfs:
    server: synology.local
    path: "/volume1/example"
```

`spec.nfs.server` is the address of your NAS on your network, in my case `synology.local`. `spec.nfs.path` is the mount path for your volume on the NAS. If you do not know what this is, on a Synology you can check it by editing your Shared Folder and selecting `NFS Permissions`. It will be shown here as `Mount path`. You should also change `spec.capacity.storage` to fit your needs.

Now, let's look at the PersistentVolumeClaim:

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: example
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 500Gi
```

The PVC is pretty straight forward. As we said earlier, this object is used to consume the storage which is made available by the PV. The PVC itself does not actually make use of your storage. In order to do that, we need to mount it into a pod. This will typically be done with a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/). Here's an example deployment that will use our NFS share:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: blog-example
  labels:
    app: blog-example
spec:
  replicas: 2
  selector:
    matchLabels:
      app: blog-example
  template:
    metadata:
      labels:
        app: blog-example
    spec:
      containers:
      - name: blog-example
        image: busybox
        command: ["/bin/sh", "-c", "while true; do ls /mnt/blog-example; sleep 10;done"]
        volumeMounts:
          - mountPath: "/mnt/blog-example"
            name: blog-example
      volumes:
        - name: blog-example
          persistentVolumeClaim:
            claimName: example
```

This deployment will create two pods, both mounting our NFS storage. This particular example will just list the content of the root of the share every 10 seconds in the logs of the pods to prove that it is sucessfully mounted.

You can see that we point to our PVC in `spec.template.spec.volumes.persistentVolumeClaim.claimName`. We then  mount that volume to `/mnt/blog-example` in `spec.template.spec.containers.volumeMounts`.

Following this model, you can easily utilize the storage from your NAS in arbitrary workloads running within Kubernetes.
